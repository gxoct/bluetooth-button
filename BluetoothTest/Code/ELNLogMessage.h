//
//  ELNLogMessage.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 29/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELNLogMessage : NSObject

@property (strong, nonatomic) NSNumber *timeStamp;
@property (strong, nonatomic) NSString *applicationState;
@property (strong, nonatomic) NSString *methodName;
@property (strong, nonatomic) NSString *className;
@property (strong, nonatomic) NSString *logDescription;

@end
