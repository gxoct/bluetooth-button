//
//  ELNIBeacon.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 19/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELNIBeacon : NSObject

- (void)startLocationManager;

@end
