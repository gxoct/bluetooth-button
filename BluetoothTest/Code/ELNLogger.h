//
//  ELNFileOperation.h
//  BluetoothTest
//
//  Created by Юрий Петухов on 18/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ELNLogMessage;

extern NSString *const kELNConnectionLogUpdateNotification;

@interface ELNLogger : NSObject

- (void)readLogWithCompletionBlock:(void(^)(NSArray *logs))completionBlock;

- (void)writeLogSelector:(SEL)aSelector class:(Class)aClass;
- (void)writeLogSelector:(SEL)aSelector class:(Class)aClass description:(NSString *)description;

- (void)subscribeLogUpdateWithCompletionBlock:(void(^)(ELNLogMessage *message))completionBlock;

- (void)removeLogs;

@end
