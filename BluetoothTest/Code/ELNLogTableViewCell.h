//
//  ELNLogTableViewCell.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 05/09/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <UIKit/UIKit.h>

static CGFloat const kELNCellHeight = 80.0;

@interface ELNLogTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *classNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *methodNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
