//
//  ELNSettingsViewController.m
//  BluetoothBeer
//
//  Created by Юрий Петухов on 05/09/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNSettingsViewController.h"
#import "ELNSettings.h"


@interface ELNSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITextField *deviceUUIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *serviceUUIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *characteristicUUIDTextField;
@property (weak, nonatomic) IBOutlet UITextField *iBeaconUUIDTextField;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end

@implementation ELNSettingsViewController

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self loadSettings];
}

- (void)setupUI {
    for (UIButton *button in self.buttons) {
        button.layer.borderWidth = 1.0;
        button.layer.cornerRadius = 5.0;
        button.layer.borderColor = [UIColor colorWithRed:0.3 green:0.5 blue:0.8 alpha:1.0].CGColor;
    }
}

#pragma mark - Settings Load/Save/Restore

- (void)loadSettings {
    self.deviceUUIDTextField.text = [[ELNSettings sharedInstance] deviceUUID];
    self.serviceUUIDTextField.text = [[ELNSettings sharedInstance] serviceUUID];
    self.characteristicUUIDTextField.text = [[ELNSettings sharedInstance] characteristicUUID];
    self.iBeaconUUIDTextField.text = [[ELNSettings sharedInstance] iBeaconUUID];
}

- (void)loadDefaultSettings {
    [[ELNSettings sharedInstance] restoreDefaultValues];
    
    [self loadSettings];
}

- (void)saveSettings {
    [ELNSettings sharedInstance].deviceUUID = self.deviceUUIDTextField.text;
    [ELNSettings sharedInstance].serviceUUID = self.serviceUUIDTextField.text;
    [ELNSettings sharedInstance].characteristicUUID = self.characteristicUUIDTextField.text;
    [ELNSettings sharedInstance].iBeaconUUID = self.iBeaconUUIDTextField.text;
}

#pragma mark - Button Actions

- (IBAction)defaultsDidTap:(UIButton *)sender {
    [self loadDefaultSettings];
}

- (IBAction)saveDidTap:(UIButton *)sender {
    [self saveSettings];
}

@end
