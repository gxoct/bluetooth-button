//
//  ELNCoreBT.m
//  BluetoothBeer
//
//  Created by Юрий Петухов on 19/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNCoreBT.h"
#import "ELNLogger.h"
#import "ELNConstants.h"

#import <CoreBluetooth/CoreBluetooth.h>

@interface ELNCoreBT() <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (nonatomic, strong) CBCentralManager *centralManager;
@property (nonatomic, strong) NSMutableArray *buttonPeripherals;

@property (strong, nonatomic) CBUUID *serviceUUID;
@property (strong, nonatomic) CBUUID *characteristicUUID;

@property (strong, nonatomic) ELNLogger *logger;

@end

@implementation ELNCoreBT

#pragma mark - Public Methods

- (void)startCentralManagerServiceUUID:(NSString *)serviceUUID characteristicUUID:(NSString *)characteristicUUID{
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    self.serviceUUID = [CBUUID UUIDWithString:serviceUUID];
    self.characteristicUUID = [CBUUID UUIDWithString:characteristicUUID];
    
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self
                                                               queue:nil
                                                             options:@{CBCentralManagerOptionRestoreIdentifierKey : kELNAppRestoreIdentifier}];
    self.buttonPeripherals = [NSMutableArray new];
}

#pragma mark - Central Manager delegate

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary<NSString *,id> *)state {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    NSArray *peripherals = state[CBCentralManagerRestoredStatePeripheralsKey];
    
    for (CBPeripheral *peripheral in peripherals) {
        [self.buttonPeripherals addObject:peripheral];
        peripheral.delegate = self;
        
        [self.centralManager connectPeripheral:peripheral options:nil];
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    if (central.state == CBCentralManagerStatePoweredOn) {
        [self.centralManager scanForPeripheralsWithServices:@[self.serviceUUID]
                                                    options:nil];
        
        for (CBPeripheral *peripheral in self.buttonPeripherals) {
            if (peripheral.state == CBPeripheralStateConnected) {
                NSUInteger serviceIDx = [peripheral.services indexOfObjectPassingTest:^BOOL(CBService *obj, NSUInteger idx, BOOL *stop) {
                    return [obj.UUID isEqual:self.serviceUUID];
                }];
                
                if (serviceIDx == NSNotFound) {
                    [peripheral discoverServices:@[self.serviceUUID]];
                    continue;
                }
                
                CBService *service = peripheral.services[serviceIDx];
                NSUInteger charIDx = [service.characteristics indexOfObjectPassingTest:^BOOL(CBCharacteristic *obj, NSUInteger idx, BOOL *stop) {
                    return [obj.UUID isEqual:self.characteristicUUID];
                }];
                
                if (charIDx == NSNotFound) {
                    [peripheral discoverCharacteristics:@[self.characteristicUUID] forService:service];
                    continue;
                }
            } else {
                // Reconnect to device
                [self.centralManager connectPeripheral:peripheral options:nil];
            }
        }
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *,id> *)advertisementData RSSI:(NSNumber *)RSSI {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    if (![self.buttonPeripherals containsObject:peripheral]) {
        [self.buttonPeripherals addObject:peripheral];
    }
    
    [self.centralManager connectPeripheral:peripheral options:nil];
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [self.logger writeLogSelector:_cmd class:[self class]];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    [self.logger writeLogSelector:_cmd class:[self class] description:[peripheral description]];

    [self.centralManager connectPeripheral:peripheral options:nil];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {
    [self.logger writeLogSelector:_cmd class:[self class] description:[peripheral description]];

    peripheral.delegate = self;
    [peripheral discoverServices:@[self.serviceUUID]];
}

#pragma mark - CBPeripheral delegate

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error {
    [self.logger writeLogSelector:_cmd class:[self class]];

    if (!error) {
        for (CBService *service in peripheral.services) {
            if ([service.UUID isEqual:self.serviceUUID]) {
                
                [peripheral discoverCharacteristics:nil
                                         forService:service];
                return;
            }
        }
    }
    
    [self.centralManager cancelPeripheralConnection:peripheral];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    if (!error) {
        for (CBCharacteristic *characteristic in service.characteristics) {
            if ([characteristic.UUID isEqual:self.characteristicUUID]) {
                [peripheral setNotifyValue:YES forCharacteristic:characteristic];
                return;
            }
        }
    }
    
    [self.centralManager cancelPeripheralConnection:peripheral];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    if (error) {
        [self.centralManager cancelPeripheralConnection:peripheral];
        return;
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(didUpdateValueForCharacteristic:)]) {
        [self.delegate didUpdateValueForCharacteristic:characteristic];
    } else {
        [self.logger writeLogSelector:_cmd class:[self class]];
    }
}

#pragma mark - Setters/Getters

- (ELNLogger *)logger {
    if (!_logger) {
        _logger = [ELNLogger new];
    }
    return _logger;
}

@end
