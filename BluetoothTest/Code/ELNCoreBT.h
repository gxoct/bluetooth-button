//
//  ELNCoreBT.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 19/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ELNCoreBTDelegate.h"

//@protocol ELNCoreBTDelegate;

@interface ELNCoreBT : NSObject

@property (nonatomic, weak) id <ELNCoreBTDelegate> delegate;

- (void)startCentralManagerServiceUUID:(NSString *)serviceUUID
                    characteristicUUID:(NSString *)characteristicUUID;

@end
