//
//  ELNLogDataSource.m
//  BluetoothBeer
//
//  Created by Юрий Петухов on 26/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNLogDataSource.h"
#import "ELNLogger.h"
#import "ELNLogMessage.h"
#import "ELNLogTableViewCell.h"

@interface ELNLogDataSource()

@property (strong, nonatomic) NSMutableArray *logMessages;
@property (weak, nonatomic) UITableView *tableView;
@property (strong, nonatomic) ELNLogger *logger;

@end

@implementation ELNLogDataSource

#pragma mark - Life Cycle

- (instancetype)initWithTableView:(UITableView *)tableView {
    self = [super init];
    if (self) {
        self.tableView = tableView;
        self.logMessages = [NSMutableArray array];
        [self loadData];
    }
    return self;
}

#pragma mark - Load Data

- (void)loadData {
    [self.logger readLogWithCompletionBlock:^(NSArray *logs) {
        NSArray *sortedMessages = [logs sortedArrayUsingComparator:^NSComparisonResult(ELNLogMessage *logA, ELNLogMessage *logB) {
            return [logB.timeStamp compare:logA.timeStamp];
        }];
        
        [self.logMessages addObjectsFromArray:sortedMessages];
        [self.tableView reloadData];
        
        [self subscribeForUpdates];
    }];
}

- (void)subscribeForUpdates {
    [self.logger subscribeLogUpdateWithCompletionBlock:^(ELNLogMessage *logMessage) {
        [self.logMessages insertObject:logMessage atIndex:0];
        [self.tableView beginUpdates];
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
        [self.tableView endUpdates];
    }];
}

#pragma mark - UITableView methods

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *LogCellIdentifier = @"ELNLogTableViewCell";
    ELNLogTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:LogCellIdentifier];
    
    if (!cell) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"ELNLogTableViewCell" owner:self options:nil][0];
    }
    
    ELNLogMessage *message = self.logMessages[(NSUInteger)indexPath.row];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[message.timeStamp doubleValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd HH:mm:ss:SSS"];
    NSString *dateInFormat = [formatter stringFromDate:date];
    
    cell.timeLabel.text = dateInFormat;
    
    NSString *applicationState;
    switch ([message.applicationState integerValue]) {
        case UIApplicationStateActive:
            applicationState = @"Active";
            break;
        case UIApplicationStateInactive:
            applicationState = @"Inactive";
            break;
        case UIApplicationStateBackground:
            applicationState = @"Background";
            break;
    }
    
    cell.stateLabel.text = applicationState;
    
    cell.methodNameLabel.text = message.methodName;
    cell.classNameLabel.text = message.className;
    cell.descriptionLabel.text = message.logDescription;
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (NSInteger)[self.logMessages count];
}

#pragma mark - Setters/Getters

- (ELNLogger *)logger {
    if (!_logger) {
        _logger = [ELNLogger new];
    }
    return _logger;
}

@end
