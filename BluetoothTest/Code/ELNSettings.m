//
//  ELNSettings.m
//  BluetoothBeer
//
//  Created by Юрий Петухов on 09/09/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNSettings.h"

#import "ELNConstants.h"

@implementation ELNSettings

@synthesize deviceUUID = _deviceUUID;
@synthesize serviceUUID = _serviceUUID;
@synthesize characteristicUUID = _characteristicUUID;
@synthesize iBeaconUUID = _iBeaconUUID;

#pragma mark - Life Cycle

+ (instancetype)sharedInstance {
    static ELNSettings *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ELNSettings alloc] init];
        [sharedInstance loadDefaultValues];
    });
    
    return sharedInstance;
}

- (void)loadDefaultValues {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults registerDefaults:@{kELNdefaultPathBTDeviceUUID : kELNdefaultBTDeviceUUID,
                                     kELNdefaultPathBTServiceUUID : kELNdefaultBTServiceUUID,
                                     kELNdefaultPathBTCharacteristicUUID : kELNdefaultBTCharacteristicUUID,
                                     kELNdefaultPathIBeaconDeviceUUID : kELNdefaultIBeaconDeviceUUID}];
    
    [userDefaults synchronize];
}

#pragma mark - Public Methods

- (void)restoreDefaultValues {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setValuesForKeysWithDictionary:@{kELNdefaultPathBTDeviceUUID : kELNdefaultBTDeviceUUID,
                                                   kELNdefaultPathBTServiceUUID : kELNdefaultBTServiceUUID,
                                                   kELNdefaultPathBTCharacteristicUUID : kELNdefaultBTCharacteristicUUID,
                                                   kELNdefaultPathIBeaconDeviceUUID : kELNdefaultIBeaconDeviceUUID}];
    
    [userDefaults synchronize];
}

#pragma mark - Setters/Getters

- (NSString *)deviceUUID {
    return _deviceUUID = [self loadObjectForKey:kELNdefaultPathBTDeviceUUID];//_deviceUUID;
}

- (void)setDeviceUUID:(NSString *)deviceUUID {
    if (deviceUUID) {
        _deviceUUID = deviceUUID;
        [self saveObject:deviceUUID forKey:kELNdefaultPathBTDeviceUUID];
    }
}

- (NSString *)serviceUUID {
    return _serviceUUID = [self loadObjectForKey:kELNdefaultPathBTServiceUUID];
}

- (void)setServiceUUID:(NSString *)serviceUUID {
    if (serviceUUID) {
        _serviceUUID = serviceUUID;
        [self saveObject:serviceUUID forKey:kELNdefaultPathBTServiceUUID];
    }
}

- (NSString *)characteristicUUID {
    return _characteristicUUID = [self loadObjectForKey:kELNdefaultPathBTCharacteristicUUID];
}

- (void)setCharacteristicUUID:(NSString *)characteristicUUID {
    if (characteristicUUID) {
        _characteristicUUID = characteristicUUID;
        [self saveObject:characteristicUUID forKey:kELNdefaultPathBTCharacteristicUUID];
    }
}

- (NSString *)iBeaconUUID {
    return _iBeaconUUID = [self loadObjectForKey:kELNdefaultPathIBeaconDeviceUUID];
}

- (void)setIBeaconUUID:(NSString *)iBeaconUUID {
    if (iBeaconUUID) {
        _iBeaconUUID = iBeaconUUID;
        [self saveObject:iBeaconUUID forKey:kELNdefaultPathIBeaconDeviceUUID];
    }
}

#pragma mark - Helper methods

- (id)loadObjectForKey:(NSString *)key {
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

- (void)saveObject:(id)object forKey:(NSString *)key {
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
