//
//  ELNFileOperation.m
//  BluetoothTest
//
//  Created by Юрий Петухов on 18/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

@import Firebase;
@import UIKit;

#import "ELNLogger.h"
#import "ELNLogMessage.h"

static NSString *const kELNCounterPath = @"CounterPath";
static NSString *const kELNRestorePath = @"RestorePath";
static NSString *const kELNConnectionLogPlist = @"ConnectionLog.plist";

static NSString * const kELNMessageTimestamp = @"timeStamp";
static NSString * const kELNMessageClassName = @"className";
static NSString * const kELNMessageMethodName = @"methodName";
static NSString * const kELNMessageApplicationState = @"applicationState";
static NSString * const kELNMEssageDescription = @"description";

static NSUInteger const kELNQueryLimit = 10;

NSString *const kELNConnectionLogUpdateNotification = @"CONNECTION_LOG_UPDATE_NOTIFICATION";

@interface ELNLogger()

@property (strong, nonatomic) FIRDatabaseReference *firDatabaseRef;

@end

@implementation ELNLogger

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.firDatabaseRef = [[FIRDatabase database] reference];
    }
    return self;
}

#pragma mark - Firebase Logs

- (void)writeLogSelector:(SEL)aSelector class:(Class)aClass description:(NSString *)description {
    [self logTest:aSelector];
    
    NSString *key = [[self.firDatabaseRef child:@"logs"] childByAutoId].key;
    NSDictionary *log = @{kELNMessageTimestamp: @([[NSDate date] timeIntervalSince1970]),
                          kELNMessageApplicationState: @([[UIApplication sharedApplication] applicationState]),
                          kELNMessageClassName: NSStringFromClass(aClass),
                          kELNMessageMethodName: NSStringFromSelector(aSelector),
                          kELNMEssageDescription: description};
    
    NSDictionary *childUpdates = @{[@"/logs/" stringByAppendingString:key] : log};
    
    [self.firDatabaseRef updateChildValues:childUpdates];
}

- (void)writeLogSelector:(SEL)aSelector class:(Class)aClass {
    [self logTest:aSelector];
    
    [self writeLogSelector:aSelector class:aClass description:@""];
}

- (void)logTest:(SEL)sel {
    NSLog(@"SEL: %@", NSStringFromSelector(sel));
}

//- (void)writeLogMessage:(ELNLogMessage *)logMessage {
//    
//    NSString *key = [[self.firDatabaseRef child:@"logs"] childByAutoId].key;
//    NSDictionary *log = @{kELNMessageTimestamp: logMessage.timeStamp,
//                          kELNMessageApplicationState: logMessage.applicationState,
//                          kELNMessageClassName: logMessage.className,
//                          kELNMessageMethodName: logMessage.methodName,
//                          kELNMEssageDescription: logMessage.description};
//    
//    NSDictionary *childUpdates = @{[@"/logs/" stringByAppendingString:key] : log};
//    
//    [self.firDatabaseRef updateChildValues:childUpdates];
//}

- (void)readLogWithCompletionBlock:(void(^)(NSArray *logs))completionBlock {
    FIRDatabaseReference *logRef = [self.firDatabaseRef child:@"logs"];
    

    [[[logRef queryOrderedByKey] queryLimitedToLast:kELNQueryLimit] observeSingleEventOfType:FIRDataEventTypeValue
                                                                                   withBlock:^(FIRDataSnapshot *snapshot) {
                                                                                       NSMutableArray *logs = [NSMutableArray array];
                                                                                       
                                                                                       for (FIRDataSnapshot *obj in snapshot.children) {
                                                                                           ELNLogMessage *message = [ELNLogMessage new];
                                                                                           
                                                                                           message.timeStamp = obj.value[kELNMessageTimestamp];
                                                                                           message.applicationState = obj.value[kELNMessageApplicationState];
                                                                                           message.className = obj.value[kELNMessageClassName];
                                                                                           message.methodName = obj.value[kELNMessageMethodName];
                                                                                           message.logDescription = obj.value[kELNMEssageDescription];
                                                                                           
                                                                                           [logs addObject:message];
                                                                                       }
                                                                                       
                                                                                       completionBlock([logs copy]);
                                                                                   }];
    
//    [[logRef queryLimitedToLast:50] observeSingleEventOfType:FIRDataEventTypeValue
//                           withBlock:^(FIRDataSnapshot *snapshot) {
//                               NSMutableArray *logs = [NSMutableArray array];
//
//                               for (FIRDataSnapshot *obj in snapshot.children) {
//                                   ELNLogMessage *message = [ELNLogMessage new];
//
//                                   message.timeStamp = obj.value[kELNMessageTimestamp];
//                                   message.applicationState = obj.value[kELNMessageApplicationState];
//                                   message.className = obj.value[kELNMessageClassName];
//                                   message.methodName = obj.value[kELNMessageMethodName];
//                                   message.logDescription = obj.value[kELNMEssageDescription];
//
//                                   [logs addObject:message];
//                               }
//
//                               completionBlock([logs copy]);
//                           }];
}

- (void)subscribeLogUpdateWithCompletionBlock:(void(^)(ELNLogMessage *message))completionBlock {
    FIRDatabaseReference *logRef = [self.firDatabaseRef child:@"logs"];

    [[[logRef queryOrderedByChild:@"logs"] queryLimitedToLast:10] observeEventType:FIRDataEventTypeChildAdded
                                                                         withBlock:^(FIRDataSnapshot *snapshot) {
                                                                             ELNLogMessage *message = [ELNLogMessage new];
                                                                             
                                                                             message.timeStamp = snapshot.value[kELNMessageTimestamp];
                                                                             message.applicationState = snapshot.value[kELNMessageApplicationState];
                                                                             message.className = snapshot.value[kELNMessageClassName];
                                                                             message.methodName = snapshot.value[kELNMessageMethodName];
                                                                             message.logDescription = snapshot.value[kELNMEssageDescription];
                                                                             
                                                                             completionBlock(message);
                                                                         }];
//    
//    [logRef observeEventType:FIRDataEventTypeChildAdded
//                   withBlock:^(FIRDataSnapshot *snapshot) {
//                       ELNLogMessage *message = [ELNLogMessage new];
//                       
//                       message.timeStamp = snapshot.value[kELNMessageTimestamp];
//                       message.applicationState = snapshot.value[kELNMessageApplicationState];
//                       message.className = snapshot.value[kELNMessageClassName];
//                       message.methodName = snapshot.value[kELNMessageMethodName];
//                       message.logDescription = snapshot.value[kELNMEssageDescription];
//
//                       completionBlock(message);
//                   }];
}

- (void)removeLogs {
    FIRDatabaseReference *logRef = [self.firDatabaseRef child:@"logs"];
    
    [self.firDatabaseRef removeValue];
    
    [logRef removeValueWithCompletionBlock:^(NSError * _Nullable error, FIRDatabaseReference * _Nonnull ref) {
        if (error) {
            NSLog(@"Error");
        } else {
            NSLog(@"Remove complete");
        }
    }];
}

//
//#pragma mark - App State
//
//+ (NSString *)addApplicationStateToString:(NSString *)string {
//    return [NSString stringWithFormat:@"%@: %@", [ELNLogger applicationState], string];
//}
//
//+ (NSString *)applicationState {
//    UIApplicationState applicationState = [[UIApplication sharedApplication] applicationState];
//    
//    NSString *applicationStateString;
//    switch (applicationState) {
//        case UIApplicationStateActive:
//            applicationStateString = @"Active";
//            break;
//        case UIApplicationStateInactive:
//            applicationStateString = @"Inactive";
//            break;
//        case UIApplicationStateBackground:
//            applicationStateString = @"Background";
//            break;
//    }
//    
//    return applicationStateString;
//}

@end
