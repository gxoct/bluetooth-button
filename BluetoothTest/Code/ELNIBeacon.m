//
//  ELNIBeacon.m
//  BluetoothBeer
//
//  Created by Юрий Петухов on 19/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNIBeacon.h"
#import "ELNLogger.h"
#import "ELNSettings.h"
#import <CoreLocation/CoreLocation.h>

#import "ELNConstants.h"

#import "ELNCoreBT.h"

@interface ELNIBeacon() <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLBeaconRegion *beaconRegion;

@property (strong, nonatomic) ELNLogger *logger;

@end

@implementation ELNIBeacon

#pragma mark - Public Methods

- (void)startLocationManager {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
    
    [self startMonitoring];
}

- (void)startMonitoring {
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
        NSString *iBeaconDeviceUUID = [[ELNSettings sharedInstance] iBeaconUUID];
        NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:iBeaconDeviceUUID];
        self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID
                                                               identifier:kELNIBeaconIdentifier];
        
        self.beaconRegion.notifyOnEntry = YES;
        self.beaconRegion.notifyOnExit = YES;
        self.beaconRegion.notifyEntryStateOnDisplay = YES;
        
        [self.locationManager startMonitoringForRegion:self.beaconRegion];
        [self.locationManager requestStateForRegion:self.beaconRegion];
    }
}

#pragma mark - Location Manager delegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    [self startMonitoring];
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    [self.locationManager requestStateForRegion:self.beaconRegion];
    
    [self.logger writeLogSelector:_cmd class:[self class]];
}

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
    
    [self.logger writeLogSelector:_cmd class:[self class]];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
    
    [self.logger writeLogSelector:_cmd class:[self class]];
}

- (void)locationManager:(CLLocationManager *)manager didRangeBeacons:(NSArray<CLBeacon *> *)beacons inRegion:(CLBeaconRegion *)region {
    [self.logger writeLogSelector:_cmd class:[self class]];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    [self.logger writeLogSelector:_cmd class:[self class] description:nil];
}

- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region {
    if (state == CLRegionStateInside) {
        [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];
    }
    
    // New state is available for monitored region
    NSString *stateString;
    switch (state) {
        case CLRegionStateUnknown:
            stateString = @"Unknown";
            break;
        case CLRegionStateInside:
            stateString = @"Inside";
            break;
        case CLRegionStateOutside:
            stateString = @"Outside";
            break;
    }
    stateString = [@"State: " stringByAppendingString:stateString];
    
    [self.logger writeLogSelector:_cmd class:[self class] description:stateString];
}

#pragma mark - Setters/Getters

- (ELNLogger *)logger {
    if (!_logger) {
        _logger = [ELNLogger new];
    }
    return _logger;
}

@end
