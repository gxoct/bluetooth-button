//
//  ELNSettings.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 09/09/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ELNSettings : NSObject

@property (strong, nonatomic) NSString *deviceUUID;
@property (strong, nonatomic) NSString *serviceUUID;
@property (strong, nonatomic) NSString *characteristicUUID;
@property (strong, nonatomic) NSString *iBeaconUUID;

+ (instancetype)sharedInstance;

- (void)restoreDefaultValues;

@end
