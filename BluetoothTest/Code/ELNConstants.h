//
//  ELNConstants.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 09/09/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kELNButtonCounter;

extern NSString * const kELNdefaultBTDeviceUUID;
extern NSString * const kELNdefaultBTServiceUUID;
extern NSString * const kELNdefaultBTCharacteristicUUID;
extern NSString * const kELNdefaultIBeaconDeviceUUID;

extern NSString * const kELNdefaultPathBTDeviceUUID;
extern NSString * const kELNdefaultPathBTServiceUUID;
extern NSString * const kELNdefaultPathBTCharacteristicUUID;
extern NSString * const kELNdefaultPathIBeaconDeviceUUID;

extern NSString * const kELNIBeaconRegion;
extern NSString * const kELNIBeaconIdentifier;

extern NSString * const kELNAppRestoreIdentifier;
