//
//  ELNLogDataSource.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 26/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ELNLogDataSource : NSObject <UITableViewDataSource>

- (instancetype)initWithTableView:(UITableView *)tableView;

@end
