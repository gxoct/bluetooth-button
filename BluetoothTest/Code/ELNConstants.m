//
//  ELNConstants.m
//  BluetoothBeer
//
//  Created by Юрий Петухов on 09/09/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNConstants.h"

NSString * const kELNButtonCounter = @"BUTTON_COUNTER";

NSString * const kELNdefaultBTDeviceUUID = @"00000000-6824-11E5-8266-0002A5D5C51B";
NSString * const kELNdefaultBTServiceUUID = @"0000D000-6824-11E5-8266-0002A5D5C51B";
NSString * const kELNdefaultBTCharacteristicUUID = @"0000D003-6824-11E5-8266-0002A5D5C51B";
NSString * const kELNdefaultIBeaconDeviceUUID = @"00000000-6824-11E5-8266-0002A5D5C51B";

NSString * const kELNdefaultPathBTDeviceUUID = @"BT_DEVICE_UUID";
NSString * const kELNdefaultPathBTServiceUUID = @"BT_SERVICE_UUID";
NSString * const kELNdefaultPathBTCharacteristicUUID = @"BT_CHARACTERISTIC_UUID";
NSString * const kELNdefaultPathIBeaconDeviceUUID = @"IBEACON_DEVICE_UUID";

NSString * const kELNIBeaconRegion = @"BEACON_REGION";
NSString * const kELNIBeaconIdentifier = @"APP_BEACON_IDENTIFIER";

NSString * const kELNAppRestoreIdentifier = @"APP_RESTORE_IDENTIFIER";
