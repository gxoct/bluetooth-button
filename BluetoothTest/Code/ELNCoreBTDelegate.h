//
//  ELNCoreBTDelegate.h
//  BluetoothBeer
//
//  Created by Юрий Петухов on 25/08/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CBCharacteristic;

@protocol ELNCoreBTDelegate <NSObject>

//- (void)didExecuteMethod:(SEL)aSelector class:(Class)aClass;
- (void)didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic;

@end
