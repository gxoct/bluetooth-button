//
//  ViewController.m
//  BluetoothTest
//
//  Created by Yuri Petukhov on 8/15/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

#import "ELNLogViewController.h"

#import <CoreBluetooth/CoreBluetooth.h>

#import "ELNConstants.h"

#import "ELNSettings.h"

#import "ELNCoreBT.h"
#import "ELNIBeacon.h"

#import "ELNLogger.h"
#import "ELNLogMessage.h"
#import "ELNLogDataSource.h"
#import "ELNLogTableViewCell.h"


@interface ELNLogViewController () <ELNCoreBTDelegate, UITableViewDelegate>

@property (strong, nonatomic) ELNCoreBT *coreBT;
@property (strong, nonatomic) ELNIBeacon *iBeacon;

@property (strong, nonatomic) NSDate *lastUpdate;
@property (strong, nonatomic) ELNLogDataSource *logDataSource;

@property (weak, nonatomic) IBOutlet UITableView *logTableView;

@property (strong, nonatomic) ELNLogger *logger;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;

@end

@implementation ELNLogViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupUI];
    
    [self.logger writeLogSelector:_cmd class:[self class]];

    self.logDataSource = [[ELNLogDataSource alloc] initWithTableView:self.logTableView];
    self.logTableView.dataSource = self.logDataSource;

    self.logTableView.delegate = self;

    [self startIBeacon];
    [self startCoreBT];
}

- (void)setupUI {
    for (UIButton *button in self.buttons) {
        button.layer.borderWidth = 1.0;
        button.layer.cornerRadius = 5.0;
        button.layer.borderColor = [UIColor colorWithRed:0.3 green:0.5 blue:0.8 alpha:1.0].CGColor;
    }
}

- (void)startCoreBT {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    self.coreBT = [ELNCoreBT new];
    self.coreBT.delegate = self;
    
    NSString *serviceUUID = [[ELNSettings sharedInstance] serviceUUID];
    NSString *chracteristicUUID = [[ELNSettings sharedInstance] characteristicUUID];
    
    [self.coreBT startCentralManagerServiceUUID:serviceUUID characteristicUUID:chracteristicUUID];
}

- (void)startIBeacon {
    [self.logger writeLogSelector:_cmd class:[self class]];
    
    self.iBeacon = [ELNIBeacon new];
    [self.iBeacon startLocationManager];
}

#pragma mark - ELNCoreBTDelegate

- (void)didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
{
    NSNumber *beerCount = [self increaseButtonCounter];
    NSString *descriptionMessage = [NSString stringWithFormat:@"Press count: %@", beerCount];
    [self.logger writeLogSelector:_cmd class:[self class] description:descriptionMessage];
}

- (NSNumber *)increaseButtonCounter {
    NSNumber *beerCount = [[NSUserDefaults standardUserDefaults] objectForKey:kELNButtonCounter];
    [[NSUserDefaults standardUserDefaults] setObject:@([beerCount integerValue] + 1) forKey:kELNButtonCounter];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    return beerCount;
}

#pragma mark - Button Actions

- (IBAction)didTapClearLog:(UIButton *)sender {
    [self.logger removeLogs];
}

#pragma mark - Table View Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kELNCellHeight;
}

#pragma mark - Setters/Getters

- (ELNLogger *)logger {
    if (!_logger) {
        _logger = [ELNLogger new];
    }
    return _logger;
}

@end
