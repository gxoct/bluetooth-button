//
//  AppDelegate.m
//  BluetoothTest
//
//  Created by Yuri Petukhov on 8/15/16.
//  Copyright © 2016 E-Legion. All rights reserved.
//

@import Firebase;

#import "AppDelegate.h"
#import "ELNLogger.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [FIRApp configure];
    [FIRDatabase database].persistenceEnabled = YES;
    
    return YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

@end
